��    J      l  e   �      P  �   Q     J  
   ]     h  
   v  2   �  5   �  6   �  9   !     [     x  ,   �     �     �     �     	     	     '	     0	     L	  )   a	     �	     �	     �	     �	     �	     �	  '   �	     
     

     
     %
     6
     ?
      B
     c
     k
     ~
     �
     �
     �
     �
     �
     �
  "   �
          &  
   <  	   G     Q     k     |     �     �  7   �     �     �                    #     (     ,     3     ;     B     G     N     T     a     i     p     y  �  �  *  *     U     j     {     �  K   �  a   �  s   ^  q   �  A   D  C   �  q   �  e   <  5   �  1   �  1   
     <     L  H   ]  *   �  h   �     :  3   L     �  (   �  Q   �       N   #     r     �  +   �  !   �     �     �  P        X  >   o  7   �     �  4   �  ,   *     W     h     z  B   �  G   �  8        X     s  /   �      �     �  H   �     7  i   R     �  1   �                    ,     4     A     P     a     |     �     �     �     �     �     �     �        <   "   )      7   0   '         8   E             *       1   &   ;      A            	       (                        
   @             >                   :      4   ?       B                    !   $               =         5       C   2   H          J           %              #   /   +               .      D       F   ,   6       9   I   3   -               G    1. Picker cell
2. Editable cell
3. Editable cell, if selected
4. Non editable cell
5. Non editable cell, if selected
Cells with duplicate numbers:
6. Editable cell
7. Editable cell, if selected
8. Non editable cell
9. Non editable cell, if selected About box (Ctrl+a) Appearance Are you sure? Background Background color for any cell that can be changed. Background color for any cell that cannot be changed. Background color for current cell that can be changed. Background color for current cell that cannot be changed. Background color for picker. Background of selected cell Background of selected, non changeable cell  Background of start numbers Bravo. Puzzle solved. Color for font Color of number if duplicate Colors: Continue Continue last game (Ctrl+c) Current preferences: Dictionary {0} not found!
Using standard. Difficulty: Drag «{}» to the board... ERROR Font color. Font name to use. Font: Foreground color for duplicate numbers. INFO Legend Not yet implemented Numbering system Numbers: OK Open preferences window (Ctrl+p) Options Options NOT saved! Options saved! QUESTION Quit application (Ctrl+q) Redo (Ctrl + Y) Refresh board Remaining numbers: Reset to defaults Return to starting form (Ctrl + Q) Show only board Show remaining pieces Show timer Standard: Start a new game (Ctrl+n) Sudoku as puzzle Undo (Ctrl + Z) Version of this window: WARNING WARNING! Wrong values for history in configuration file You are about to:
 You have placed all «{}». _Cancel _No _OK _Yes any arabic chinese custom easy expert greek intermediate persian simple standard tibetan Project-Id-Version: Sudoku as puzzle
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
Last-Translator: 
Language: el
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: ../olds
X-Poedit-SearchPathExcluded-1: ../__pycache__
 1. Κελί επιλογέα.
2. Επεξεργάσιμο κελί.
3. Επιλεγμένο κελί αν είναι επεξεργάσιμο.
4. Μη επεξεργάσιμο κελί.
5. Επιλεγμένο κελί αν δεν είναι επεξεργάσιμο.
Κελιά με διπλούς αριθμούς:
6. Επεξεργάσιμο κελί.
7. Επιλεγμένο κελί αν είναι επεξεργάσιμο.
8. Μη επεξεργάσιμο κελί.
9. Επιλεγμένο κελί αν δεν είναι επεξεργάσιμο. Περί... (Ctrl+a) Εμφάνιση Είστε σίγουρη/ος; Παρασκήνιο Χρώμα παρασκηνίου για επεξεργάσιμο κελί. Χρώμα παρασκηνίου για κελί που δεν μπορεί να αλλάξει. Χρώμα παρασκηνίου για το τρέχον κελί, εφόσον μπορεί να αλλάξει. Χρώμα παρασκηνίου για το τρέχον κελί αν δεν μπορεί να αλλάξει. Χρώμα παρασκηνίου για τον επιλογέα. Χρώμα παρασκηνίου για το τρέχον κελί Χρώμα παρασκηνίου για το τρέχον κελί αν δεν μπορεί να αλλάξει. Χρώμα παρασκηνίου για τα αρχικά νούμερα του παιχνιδιού Μπράβο! Ολοκληρώσατε το παζλ. Χρώμα για τη γραμματοσειρά Χρώμα για διπλούς αριθμούς Χρώματα: Συνέχεια Συνεχίζει το προηγούμενο παιχνίδι (Ctrl+c) Τρέχουσες προτιμήσεις: Το λεξικό για {0} δεν βρέθηκε!
Θα χρησιμοποιηθεί το βασικό. Δυσκολία: Σύρετε το «{}» στο παράθυρο... ΣΦΑΛΜΑ Χρώμα γραμματοσειράς. Όνομα γραμματοσειράς που θα χρησιμοποιηθεί. Γραμματοσειρά Χρώμα γραμματοσειράς αν υπάρχει δύο φορές. ΠΛΗΡΟΦΟΡΙΕΣ Υπόμνημα Δεν έχει φτιαχτεί ακόμα Σύστημα αρίθμησης Αριθμοί: Εντάξει Ανοίγει το παράθυρο με τις προτιμήσεις (Ctrl+p) Προτιμήσεις Οι προτιμήσεις ΔΕΝ αποθηκεύτηκαν! Οι προτιμήσεις αποθηκεύτηκαν! ΕΡΩΤΗΣΗ Έξοδος από την εφαρμογή (Ctrl+q) Ακύρωση αναίρεσης (Ctrl + Y) Ανανέωση Υπόλοιπα: Επαναφορά στα Επιστροφή στο αρχικό παράθυρο (Ctrl + Q) Δείξε μόνο το τετράγωνο του παιχνιδιού Δείξε τα νούμερα που απομένουν Δείξε το ρολόι Βασικό: Αρχίζει νέο παιχνίδι (Ctrl+n) Σουντόκου ως παζλ Αναίρεση (Ctrl + Z) Έκδοση του αρχείου αυτού του παραθύρου: ΠΡΟΕΙΔΟΠΟΙΗΣΗ ΠΡΟΣΟΧΗ! Λάθος τιμές στο ιστορικό στο αρχείο προτιμήσεων. Πρόκειται να:
 Έχετε προσθέσει όλα τα «{}». Ά_κυρο Ό_χι Ε_ντάξει _Ναι τυχαία αραβικά κινεζικά προσαρμοσμένο μεσαίο πολύ δύσκολο ελληνικά δύσκολο περσικά εύκολο βασικό θιβετιανά 